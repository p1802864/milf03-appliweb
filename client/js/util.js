function completeTemplate(idScriptTemplate,idSection,data) {
        console.debug("Completing Template...");
        console.log(data);
        const template = document.getElementById(idScriptTemplate).innerHTML;
        var rendered = Mustache.render(template,data);
        document.getElementById(idSection).innerHTML = rendered;
}

function verifyVote() {
        getMyVote()
            .then(data => {
            	completeTemplate('templateForBallot','ballot',{hasVoted:true });

                const suppForm = document.getElementById('suppForm');
                suppForm.onsubmit = (ev) => {
                    ev.preventDefault();
                    supprVote(data['id']).then(() => verifyVote());
                }
            })
            .catch(err => completeTemplate('templateForBallot','ballot',{hasVoted:false}));
}

function outsideClick(event, notelem)	{
    notelem = $(notelem);
    var clickedOut = true, i, len = notelem.length;
    for (i = 0;i < len;i++)  {
        if (event.target == notelem[i] || notelem[i].contains(event.target)) {
            clickedOut = false;
        }
    }
    if (clickedOut) return true;
    else return false;
}

function verifyConnect(admin) {
    if(state.token == ''){
        $('.needConnect').addClass('inactive');
        $('.needUnconnect').removeClass('inactive');
    }
    else{
        $('.needConnect').removeClass('inactive');
        $('.needUnconnect').addClass('inactive');

        verifyVote();
                
        completeTemplate('templateForMonCompte','monCompte',{
            "login": `${state.login}`,
            "nom": `${state.nom}`,
            "admin": (admin?true:false) // c'est un peu de la triche mais c'est pas grave on fait l'appli d'un non-admin
            });
        const changeNom = document.getElementById('editable');
        changeNom.setAttribute('contentEditable','true');
        window.addEventListener('click', (event) => {
        	if(outsideClick(event, changeNom)){
        		name = changeNom.innerHTML;
        		if(name != state.nom){
        			
        		}
        	}
        })
    }
}