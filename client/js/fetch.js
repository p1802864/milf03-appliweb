const state = {
	token: '',//token vide si l'utilisateur n'est pas connecté

	apiUrl: 'https://192.168.75.56/api/v3',

	origin: 'http://localhost:8080',

	headers(){
		const headers = new Headers();
		headers.set('Accept', 'application/json');
		headers.set('Content-Type', 'application/json');
		headers.set('Origin',this.origin);
		if(this.token != ''){
			headers.set('Authorization', this.token);
		}
		return headers;
	}
}

function filtreErreur(response) {
	if(response.status>=400 && response.status < 600) {
		throw new Error(`erreur ${response}`);
	}
	return response;
}

function toJson(response) {
	return response
		.json()
		.then(data => data)//return l'objet json
		.catch(err=>console.error(`Error on json: ${err}`));
}

const connect = (login,name,admin) => {//normalement admin devrait toujours être false
	return fetch(
		`${state.apiUrl}/users/login`,
		{
			method: 'POST',
			body:`{"login":"${login}","nom":"${name}","admin":${admin}}`,
			headers: state.headers()
		})
		.then(filtreErreur)
		.then( r => {
			state.token = r.headers.get('Authorization').split(' ')[1];
			state.login = login;
			state.nom = name;
			return true; //utilisateur connecté
			});
}

const disconnect = () => {
	return fetch(
		`${state.apiUrl}/users/logout`,
		{
			method: 'POST',
			headers: state.headers()
		})
		.then(filtreErreur)
		.then( r => {state.token = ''; return true;});
}

const getResultats  = () => {
	return fetch(
		`${state.apiUrl}/election/resultats`,
		{
			method: 'GET',
			headers: state.headers()
		})
		.then(filtreErreur)
		.then(toJson)
}


const getCandidats = () => {
	return fetch(
		`${state.apiUrl}/election/candidats`,
		{
			method: 'GET',
			headers: state.headers()
		})
		.then(filtreErreur)
		.then(toJson);
}

const getNomsCandidats = () => {
	return fetch(
		`${state.apiUrl}/election/candidats/noms`,
		{
			method: 'GET',
			headers: state.headers()
		})
		.then(filtreErreur)
		.then(toJson);
}

const getCandidat = (id) => {
	return fetch(
		`${state.apiUrl}/election/candidats/${id}`,
		{
			method: 'GET',
			headers: state.headers()
		})
		.then(filtreErreur)
		.then(toJson);
}

const vote = (candidat) => {
	return fetch(
		`${state.apiUrl}/election/ballots/`,
		{
			method: 'POST',
			headers: state.headers(),
			body: `{"nomCandidat": "${candidat}"}`
		})
		.then(filtreErreur);
}

const supprVote = (id) => {
	return fetch(
		`${state.apiUrl}/${id}`,
		{
			method: 'DELETE',
			headers: state.headers()
		})
		.then(filtreErreur);
}

const getMyVote = () => {
	return fetch(
		`${state.apiUrl}/election/ballots/byUser/${state.login}`,
		{
			method: 'GET',
			headers: state.headers()
		})
		.then(filtreErreur)
		.then(toJson);//.then pour récup les données .catch pour quand l'utilisateur n'a pas voté 
}


const changeName = (name) => {
	return fetch(
		`${state.apiUrl}/users/${state.login}/nom`,
		{
			method: 'PUT',
			headers: state.headers(),
			body: `{"nom":"${name}"}`
		})
		.then(filtreErreur).then(state.nom = name);
}
