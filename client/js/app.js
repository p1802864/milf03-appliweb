jQuery(document).ready(function($) {



    $('.toggleTrigger').click(function(){
        $('.nav').slideToggle();
        $('#hamburger').toggleClass('trigger');
        });

        window.addEventListener(
        'hashchange',
        () => { show(window.location.hash); }
        );

        function show(hash) {
            const h = hash.split("/")
            $('.active')
            .removeClass('active')
            .addClass('inactive');
            $(h[0])
            .removeClass('inactive')
            .addClass('active');
            if(h[1]) {
                getCandidat(window.location.hash.split("/")[1])
    
                    .then(data => completeTemplate('templateForCandidat','candidat',data))
                    .catch(err => completeTemplate('templateForCandidat', 'candidat', {"prenom": "vous devez être connecté.e.s pour voir ce contenue","nom": "!"}));
            }
        }

        verifyConnect();

        const elements = document.getElementById('nav').children;
        for (var i = elements.length - 1; i >= 0; i--) {
            const e = elements[i].children[0];
            e.onclick = () => {
                $('.current').removeClass('current');   
                e.classList.add('current');
            }
        }



    function refreshResultat()
    {
        getResultats().then(data=>completeTemplate('templateForResultats','index',{array_of_strings : data}));
    }

    setInterval(()=>
    {
        refreshResultat()
    }, 5000); //5 seconde en ms



});