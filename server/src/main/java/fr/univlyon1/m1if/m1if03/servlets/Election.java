package fr.univlyon1.m1if.m1if03.servlets;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet("/election/*")
public class Election extends HttpServlet {
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getRequestURI().replace(req.getContextPath() + "/election/", "");
        req.setAttribute("action", action); // Utilisé dans electionHome.jsp
        List<String> actions = Arrays.asList(action.split("\\s*/\\s*"));
        switch(actions.get(0)) {
            case "resultats":
                this.getServletContext().getNamedDispatcher("Resultats").forward(req, resp);
                break;
            case "candidats":
                this.getServletContext().getNamedDispatcher("Candidats").forward(req,resp);
                break;
            case "ballots":
                this.getServletContext().getNamedDispatcher("Ballots").forward(req, resp);
                break;
            case "vote":
                this.getServletContext().getNamedDispatcher("Vote").forward(req, resp);
                break;
            case "votes":
                this.getServletContext().getNamedDispatcher("Votes").forward(req, resp);
                break;
            default:
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
