package fr.univlyon1.m1if.m1if03.servlets;

import fr.univlyon1.m1if.m1if03.DTOs.CandidatDTO;
import fr.univlyon1.m1if.m1if03.DTOs.URLList;
import fr.univlyon1.m1if.m1if03.classes.Candidat;
import fr.univlyon1.m1if.m1if03.utils.CandidatListGenerator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@WebServlet(name="Candidats", urlPatterns = {})
public class Candidats extends HttpServlet {

    
    Map<String, Candidat> candidats;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getRequestURI().replace(req.getContextPath() + "/election/", "");
        candidats = (Map<String, Candidat>) req.getServletContext().getAttribute("candidats");
        //req.setAttribute("action", action);
        List<String> actions = Arrays.asList(action.split("\\s*/\\s*"));
        candidats = (Map<String, Candidat>) req.getServletContext().getAttribute("candidats");
        
        if(action.endsWith("candidats")) { 
            String url = req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort()+req.getContextPath()+"/election/candidats/";
            req.setAttribute("dto", new URLList(url, candidats.keySet()));
            resp.setStatus(HttpServletResponse.SC_OK);
        }   
        else {
            switch(actions.get(1)) {
                case "noms" :
                    String url = "";
                    req.setAttribute("dto", new URLList(url, candidats.keySet()));
                    resp.setStatus(HttpServletResponse.SC_OK);
                    break;
                default :
                    Candidat candidat = candidats.get(actions.get(1));
                    if(candidat == null) {
                        resp.sendError(HttpServletResponse.SC_NOT_FOUND);
                        return;
                    }
                    req.setAttribute("dto", new CandidatDTO(candidat));
                    resp.setStatus(HttpServletResponse.SC_OK);
                    break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getRequestURI().replace(req.getContextPath() + "/election/", "");
        if(action.endsWith("/update")) {
            try {
                if (candidats == null) {
                    candidats = CandidatListGenerator.getCandidatList();
                    req.getServletContext().setAttribute("candidats", candidats);
                    System.out.println(candidats.size() + " candidats.");
                }
            } catch (IOException e) {
                e.printStackTrace();
                resp.sendError(500);
            }
            resp.sendError(HttpServletResponse.SC_NO_CONTENT);
        }
        else {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);//post avec autre chose que update
        }
    }
}