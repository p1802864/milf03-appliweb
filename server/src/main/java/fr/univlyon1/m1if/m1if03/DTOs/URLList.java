package fr.univlyon1.m1if.m1if03.DTOs;

import java.util.Set;

public class URLList implements DTO{

    private String url;
    private Set<String> names;

    public URLList(String u,Set<String> n) {
        url = u;
        names = n;
    }

    public Set<String> getNames(){
        return names;
    }
    public String getURL(){
        return url;
    }

    @Override
    public String getJSP() {
        return "nameList.jsp";
    }

    @Override
    public String getJSON() {
        String ret = "[";
        for (String name : names) {
            ret+="\""+url+name+"\",";
        }
        return ret.replaceFirst(".$","]");
    }
}
