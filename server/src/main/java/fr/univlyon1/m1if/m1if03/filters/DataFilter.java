package fr.univlyon1.m1if.m1if03.filters;

import fr.univlyon1.m1if.m1if03.DTOs.DTO;
import fr.univlyon1.m1if.m1if03.utils.BufferlessHttpServletResponseWrapper;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebFilter(filterName = "DataFilter", urlPatterns = {"/election/*","/users/*"})
public class DataFilter extends HttpFilter{
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse wrapper = new BufferlessHttpServletResponseWrapper(res);
        super.doFilter(req, wrapper, chain);

        if(req.getMethod().equals("GET") && res.getStatus() == HttpServletResponse.SC_OK) {
            DTO data = (DTO) req.getAttribute("dto");
            if(data!= null){
                if(req.getHeader("accept").contains("application/json")) { //par défaut json sinon html sinon not acceptable
                    res.setContentType("application/json");
                    PrintWriter out = res.getWriter();
                    out.println(data.getJSON());
                    
                }
                else if(req.getHeader("accept").contains("text/html")) { 
                    res.setContentType("text/html");
                    req.getRequestDispatcher("/WEB-INF/components/"+data.getJSP()).forward(req, res);
                }
                else{
                    res.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
                }
            }
        }
    }
}
