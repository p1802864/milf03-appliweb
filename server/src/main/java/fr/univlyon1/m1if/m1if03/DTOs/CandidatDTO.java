package fr.univlyon1.m1if.m1if03.DTOs;

import fr.univlyon1.m1if.m1if03.classes.Candidat;

public class CandidatDTO implements DTO{

    private String nom;
    private String prenom;

    public CandidatDTO(Candidat c){
        nom = c.getNom();
        prenom = c.getPrenom();
    }
    
    public String getPrenom() {
        return prenom;
    }
    public String getNom() {
        return nom;
    }

    @Override
    public String getJSP(){
        return "candidat.jsp";
    }

    @Override
    public String getJSON(){
        return  "{ \"prenom\" : \""+prenom+
                "\", \"nom\" : \""+nom+"\"}";
    }
}
