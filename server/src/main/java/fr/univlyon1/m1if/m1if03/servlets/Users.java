package fr.univlyon1.m1if.m1if03.servlets;

import javax.el.BeanNameResolver;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.univlyon1.m1if.m1if03.DTOs.URLList;
import fr.univlyon1.m1if.m1if03.DTOs.UserDTO;
import fr.univlyon1.m1if.m1if03.classes.User;
import fr.univlyon1.m1if.m1if03.utils.JWThelper;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@WebServlet("/users/*")
public class Users extends HttpServlet {
    HashMap<String, User> users= new HashMap<>();
    @Override
    @SuppressWarnings("unchecked")
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        users = (HashMap<String, User>) config.getServletContext().getAttribute("users");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getRequestURI().replace(req.getContextPath()+"/", "");
        List<String> actions = Arrays.asList(action.split("\\s*/\\s*"));
        User u = ((User) req.getAttribute("user"));
        String url = req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort()+req.getContextPath();
        if(action.endsWith("users")){
            req.setAttribute("dto",new URLList(url+"/users/",users.keySet()));
            resp.setStatus(HttpServletResponse.SC_OK);
            return;
        }
        else if(actions.size() <= 2) {
            if(u.isAdmin()||u.getLogin()==actions.get(1)) {
                if(users.get(actions.get(1))==null) {
                    resp.sendError(HttpServletResponse.SC_NOT_FOUND,"Cet utilisateur n'existe pas !");
                    return;
                }
                req.setAttribute("dto", new UserDTO(users.get(actions.get(1))));
                resp.setStatus(HttpServletResponse.SC_OK);
                return;
            }
            else {
                resp.sendError(HttpServletResponse.SC_FORBIDDEN, "vous devez être administrateur ou être l'utilisateur concercé ("+actions.get(1)+")!");
                return;
            }
        }
        else {
            switch(actions.get(2)) {
                case "ballot":
                    if(u.isAdmin()||u.getLogin()==actions.get(1)) {
                        resp.sendRedirect(url+"/election/ballots/byUser/"+actions.get(1));
                    }
                    else{
                        resp.sendError(HttpServletResponse.SC_FORBIDDEN,"vous devez soit être administrateur soit être propriétaire de ce ballot !");
                        return;
                    }
                    break;
                case "vote":
                    if(u.isAdmin()||u.getLogin()==actions.get(1)) {
                        resp.sendRedirect(url+"/election/votes/byUser/"+actions.get(1));
                    }
                    else{
                        resp.sendError(HttpServletResponse.SC_FORBIDDEN,"vous devez soit être administrateur soit être propriétaire de ce vote !");
                        return;
                    }
                    break;
                default:
                    resp.sendError(HttpServletResponse.SC_NOT_FOUND,"Cette action ("+actions.get(2)+") n'existe pas !");
                    break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getRequestURI().replace(req.getContextPath()+"/", "");
        List<String> actions = Arrays.asList(action.split("\\s*/\\s*"));
        switch(actions.get(1)) {
            case "login":
                String login = req.getParameter("login");
                if(login == null){
                    resp.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
                    return;
                }
                String token = JWThelper.generateToken(login, req.getParameter("admin") != null && req.getParameter("admin").equals("on"), req);
                resp.setHeader("Authorization","Bearer "+token);
                resp.sendError(HttpServletResponse.SC_NO_CONTENT);
                break;
            case "logout":
                
                resp.sendError(HttpServletResponse.SC_NO_CONTENT);
                break;
            
            default:
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getRequestURI().replace(req.getContextPath()+"/", "");
        List<String> actions = Arrays.asList(action.split("\\s*/\\s*"));
        User u = ((User) req.getAttribute("user"));
        if(actions.get(2)!=null&&actions.get(2)=="nom") {
            if(u.isAdmin()||u.getLogin()==actions.get(1)) {
                if(users.get(actions.get(1))==null) {
                    resp.sendError(HttpServletResponse.SC_NOT_FOUND,"Cet utilisateur n'existe pas !");
                    return;
                }
                if(req.getParameter("nom")==null) {
                    resp.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
                    return;
                }
                users.get(actions.get(1)).setNom(req.getParameter("nom"));
                if( u.getLogin()==actions.get(1)) {
                    u.setNom(req.getParameter("nom"));
                }
                resp.sendError(HttpServletResponse.SC_NO_CONTENT);
            }
            else {
                resp.sendError(HttpServletResponse.SC_FORBIDDEN, "vous devez être administrateur ou être l'utilisateur concerné !");
                return;
            }
        }
        else{
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
