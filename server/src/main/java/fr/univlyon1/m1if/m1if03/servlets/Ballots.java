package fr.univlyon1.m1if.m1if03.servlets;

import fr.univlyon1.m1if.m1if03.classes.Ballot;
import fr.univlyon1.m1if.m1if03.classes.Candidat;
import fr.univlyon1.m1if.m1if03.classes.User;


import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebServlet(name="Ballots", urlPatterns = {})
public class Ballots extends HttpServlet {
    Map<Integer, Ballot> ballots;
    Map<String,Integer> userId;


    @Override
    @SuppressWarnings("unchecked")
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ballots = (Map<Integer, Ballot>) config.getServletContext().getAttribute("ballots");
        userId=(Map<String,Integer> ) config.getServletContext().getAttribute("userId");


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getRequestURI().replace(req.getContextPath() + "/election/ballots", "");
        req.setAttribute("action", action); // Utilisé dans electionHome.jsp
        List<String> actions = Arrays.asList(action.split("\\s*/\\s*"));    
        String url = req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort()+req.getContextPath()+"/election/";    
        User u=(User) req.getAttribute("user");
        if(actions.size()==1){
            if(u.isAdmin()){
                ArrayList<String> urlOfBallots=new ArrayList<>();
                for (Map.Entry mapentry : ballots.entrySet()) {
                    urlOfBallots.add(url+"ballots/"+mapentry.getKey());
                    }
                req.setAttribute("urlOfBallots", urlOfBallots);
                req.getRequestDispatcher("/WEB-INF/components/listBallots.jsp").forward(req, resp);
            }
            else{
                resp.sendError(HttpServletResponse.SC_FORBIDDEN, "User is not Admin");
            }

        }
        else if(actions.get(1).equals("byUser") && actions.size()==3){
            if(u.isAdmin() || userId.get(u.getLogin())==userId.get(actions.get(2))){
                req.setAttribute("apiResultOfByUser", url+"ballots/"+userId.get(actions.get(2)));
                req.getRequestDispatcher("/WEB-INF/components/ballotUser.jsp").forward(req, resp);
            } else{
                resp.sendError(HttpServletResponse.SC_FORBIDDEN, "User is neither Admin or owner of the ballots");
            }
        }
        else {
            if(u.isAdmin() || userId.get(u.getLogin())==userId.get(actions.get(1))){
                req.setAttribute("apiResultOfBallotId", url+"vote/"+actions.get(1));
                req.getRequestDispatcher("/WEB-INF/components/ballotUser.jsp").forward(req, resp);
            } else{
                resp.sendError(HttpServletResponse.SC_FORBIDDEN, "User is neither Admin or owner of the ballots");
            }

        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("Vote.java").forward(req, resp);
    }

    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User u=(User) req.getAttribute("user");
        String action = req.getRequestURI().replace(req.getContextPath() + "/election/ballots", "");
        List<String> actions = Arrays.asList(action.split("\\s*/\\s*"));    
        if(actions.get(1)==""+userId.get(u.getLogin()) || u.isAdmin()){
            req.getRequestDispatcher("DeleteVote.java").forward(req, resp);
        }
    }


}