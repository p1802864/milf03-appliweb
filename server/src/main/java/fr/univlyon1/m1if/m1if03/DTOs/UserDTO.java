package fr.univlyon1.m1if.m1if03.DTOs;

import fr.univlyon1.m1if.m1if03.classes.User;

public class UserDTO implements DTO {
    private String login;
    private String nom;
    private boolean admin;

    public UserDTO(User u) {
        login = u.getLogin();
        nom = u.getNom();
        admin = u.isAdmin();
    }
    public String getNom() {
        return nom;
    }
    public boolean isAdmin() {
        return admin;
    }
    public String getLogin() {
        return login;
    }

    @Override
    public String getJSP() {
        return "user.jsp";
    }

    @Override
    public String getJSON() {
        return  "{ \"login\":"+login+
                ", \"nom\":"+nom+
                ", \"admin\":"+admin+"}";
    }
}
