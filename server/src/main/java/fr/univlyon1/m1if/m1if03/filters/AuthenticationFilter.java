package fr.univlyon1.m1if.m1if03.filters;

import fr.univlyon1.m1if.m1if03.classes.User;
import fr.univlyon1.m1if.m1if03.utils.JWThelper;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;

import com.auth0.jwt.exceptions.JWTVerificationException;

import java.io.IOException;
import java.util.HashMap;

@WebFilter(filterName = "AuthenticationFilter", urlPatterns = "/*")
public class AuthenticationFilter extends HttpFilter {
    private final String[] authorizedURIs = {"/index.html", 
                                             "/static/vote.css", 
                                             "/election/resultats", 
                                             "/election/candidats/noms",
                                             "/election/candidats",
                                             "/users/login"}; // Manque "/", traité plus bas...
    @Override
    @SuppressWarnings("unchecked")
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String currentUri = req.getRequestURI().replace(req.getContextPath(), "");

        // Traitement de l'URL racine
        if(currentUri.equals("/")) {
            res.sendRedirect("index.html");
            return;
        }

        // Traitement des autres URLs autorisées sans authentification
        for(String authorizedUri: authorizedURIs) {
            if(currentUri.equals(authorizedUri)) {
                super.doFilter(req, res, chain);
                return;
            }
        }

        String token = req.getHeader("Authorization"); // On récupère le token
        if(token != null) {
            String tok = token.replace("Bearer ", "");
            try {
                String sub = JWThelper.verifyToken(tok, req);
                User user = ((HashMap<String,User>) req.getServletContext().getAttribute("users")).get(sub);
                req.setAttribute("user", user );
                super.doFilter(req, res, chain);
                return;
            }
            catch(JWTVerificationException e) {
                res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "connexion invalide ou expirée");
            }
        }
        else {
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Vous devez être connecté pour accéder à cette page.");
        }
    }
}