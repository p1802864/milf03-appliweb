package fr.univlyon1.m1if.m1if03.servlets;

import fr.univlyon1.m1if.m1if03.classes.Ballot;
import fr.univlyon1.m1if.m1if03.classes.Bulletin;
import fr.univlyon1.m1if.m1if03.classes.Candidat;
import fr.univlyon1.m1if.m1if03.classes.User;


import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Map;
import java.util.Arrays;
import java.util.List;

@WebServlet(name="Votes", urlPatterns = {})
public class Votes extends HttpServlet {
    Map<Integer, Ballot> ballots;
    Map<String,Integer> userId;
    Map<String, Candidat> candidats;


    @Override
    @SuppressWarnings("unchecked")
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ballots = (Map<Integer, Ballot>) config.getServletContext().getAttribute("ballots");
        userId=(Map<String,Integer> ) config.getServletContext().getAttribute("userId");
        candidats = (Map<String, Candidat>) config.getServletContext().getAttribute("candidats");

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getRequestURI().replace(req.getContextPath() + "/election/votes", "");
        req.setAttribute("action", action); // Utilisé dans electionHome.jsp
        List<String> actions = Arrays.asList(action.split("\\s*/\\s*"));    
        User u=(User) req.getAttribute("user");
        String url = req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort()+req.getContextPath()+"/election/";    

        if(actions.get(1).equals("byUser") && actions.size()==3){
            if(u.isAdmin() || userId.get(u.getLogin())==userId.get(actions.get(2))){
                req.setAttribute("apiResultOfByUser", url+"votes/"+userId.get(actions.get(2)));
                req.getRequestDispatcher("/WEB-INF/components/ballotUser.jsp").forward(req, resp);
            } else{
                resp.sendError(HttpServletResponse.SC_FORBIDDEN, "User is neither Admin or owner of the ballots");
            }
        }
        else{
            if(u.isAdmin() || (""+userId.get(u.getLogin())).equals(actions.get(1))){
                req.setAttribute("apiResultOfVote","{}");
                req.getRequestDispatcher("/WEB-INF/components/ballotUser.jsp").forward(req, resp);
            }
         else{
            resp.sendError(HttpServletResponse.SC_FORBIDDEN, "");
         }
        }
    }


    protected  void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getRequestURI().replace(req.getContextPath() + "/election/votes", "");
        req.setAttribute("action", action); // Utilisé dans electionHome.jsp
        List<String> actions = Arrays.asList(action.split("\\s*/\\s*"));    
        User u=(User) req.getAttribute("user");
        String url = req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort()+req.getContextPath()+"/election/";    

        if(actions.get(1).equals("byUser") && actions.size()==3 && req.getParameter("candidat") != null){
            if(u.isAdmin() || userId.get(u.getLogin())==userId.get(actions.get(2))){
                if(candidats.get(req.getParameter("candidat"))!=null){
                    Bulletin b=new Bulletin(candidats.get(req.getParameter("candidat")));
                    ballots.get(userId.get(actions.get(2))).setBulletin(b);
                    req.getRequestDispatcher("/WEB-INF/components/ballotUser.jsp").forward(req, resp);
                } else{
                    resp.sendError(HttpServletResponse.SC_BAD_REQUEST,"");
                }
               
            } else{
                resp.sendError(HttpServletResponse.SC_FORBIDDEN, "User is neither Admin or owner of the ballots");
        }
        }
        }
        
    
    }



