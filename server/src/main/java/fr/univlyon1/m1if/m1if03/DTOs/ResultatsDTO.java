package fr.univlyon1.m1if.m1if03.DTOs;

import java.util.Map;
import java.util.Map.Entry;

public class ResultatsDTO implements DTO{
    private Map<String,Integer> res;

    public ResultatsDTO(Map<String,Integer> r){
        res = r;
    }

    public Map<String,Integer> getRes(){
        return res;
    }

    @Override
    public String getJSP(){
        return "resultats.jsp";
    }

    @Override
    public String getJSON(){
        String ret = "[";
        for (Entry<String, Integer> resultat : res.entrySet()) {
            ret +=  "{ \"nomCandidat\":\""+resultat.getKey()+
                    "\", \"votes\":"+resultat.getValue().toString()+"},";
        }
        return ret.replaceFirst(".$","]");
    }
}
