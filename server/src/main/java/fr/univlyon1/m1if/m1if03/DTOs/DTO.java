package fr.univlyon1.m1if.m1if03.DTOs;

public interface DTO {
    public String getJSP();//renvoie le chemin vers la jsp correspondant à la dto
    public String getJSON();//renvoie l'objet serialisé en json
}
