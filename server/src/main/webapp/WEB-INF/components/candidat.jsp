<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<jsp:useBean id="dto" type="fr.univlyon1.m1if.m1if03.DTOs.CandidatDTO" scope="request" beanName="dto"/>
<html>
<head>
    <title>Candidats</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/vote.css">
</head>
<body>
<jsp:include page="title.jsp?title=Candidats à l'élection"/>
<main id="contenu" class="wrapper">
    <jsp:include page="menu.jsp" />
    <article class="contenu">
        <ul>
            <li>Prenom : <%=dto.getPrenom()%></li>
            <li>Nom : <%=dto.getNom()%></li>
        </ul>
    </article>
</main>
</body>
</html>