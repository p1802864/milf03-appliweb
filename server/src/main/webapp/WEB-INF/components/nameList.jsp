<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Candidats</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/vote.css">
</head>
<body>
<jsp:include page="title.jsp?title=Candidats à l'élection"/>
<main id="contenu" class="wrapper">
    <jsp:include page="menu.jsp" />
    <article class="contenu">
        <ul>
            <c:forEach items="${requestScope.dto.getNames()}" var="name">
                <li><c:out value="${requestScope.dto.getURL()}"/><c:out value="${name}"/></li>
            </c:forEach>
        </ul>
    </article>
</main>
</body>
</html>