<%--
  Created by IntelliJ IDEA.
  User: Lionel
  Date: 09/10/2021
  Time: 22:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:useBean id="dto" type="fr.univlyon1.m1if.m1if03.DTOs.UserDTO" beanName="dto" scope="request"/>
<html>
<head>
    <title>User <%= dto.getLogin()%>
    </title>
    <base href="..">
    <link rel="stylesheet" type="text/css" href="static/vote.css">
</head>
<body>
<jsp:include page="title.jsp?title=Votre compte"/>
<main id="contenu" class="wrapper">
    <jsp:include page="menu.jsp"/>
    <article class="contenu">
        <ul>
            <li>login : <%=dto.getLogin()%></li>
            <li>nom : <%=dto.getNom()%></li>
            <li>admin : <%=dto.isAdmin()%></li>
        </ul>
    </article>
</main>
</body>
</html>