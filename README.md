# MIF03-Conception d'application Web

GIT contenant toute les réalisations effectuées pendant les TPs de l'UE **MIF03-Conception d'application Web**

Par le binôme composé de :
* CORTINOVIS Wesley 11803391
* BRESSAT JONATHAN 11802864

Ce **readme** contient aussi les différents choix d'implémentation que nous avons choisis, ainsi que certains détails utiles.

[Un petit peu de musique ?](https://www.youtube.com/watch?v=dQw4w9WgXcQ)

## TP N°2 - Programmation côté serveur

Pour faire la redirection vers la page d'acceuil lorsque la Session est inexistante (utilisateur non connecté) il suffit d'utiliser :
```
<c:if test="${sessionScope.user == null}">
<meta http-equiv="refresh" content="0; URL=./index.html"/>
</c:if>
``` 
Dans la balise `<head>` du fichier jsp

**Déconnexion**

Créez une servlet Deco chargée de déconnecter l'utilisateur en supprimant ses attributs de session et de le renvoyer vers la page d'accueil.

Quel est le type de redirection que vous devez employer pour cela .
> Nous utilisons la méthode sendRedirect(), il s'agit donc d'une redirection exécutée côté client.


**Vote Blanc**

Pour voter blanc la meilleur solution que nous avons trouvé est de mettre en place un membre _blank dans la liste des candidats.

## TP N°3 - Design patterns côté serveur en Java

**Pattern MVC**

Au final, à quoi sert le contrôleur principal ?.
>Après les modifications, le contrôleur pricipal ne sert qu'à lire le lien qu'il reçois pour renvoyer vers d'autre contrôlleur. Le contrôleur principal nert plus vraiment à notre application.

Mise en place des différents contrôleur : 
>Les contrôleur ne sont autre que les Servlets du TP2 avec de legère modification.

Header Last-Modified :
>On a décidé d'utiliser la date courante comme recommandé sur l'ennoncé du TP.

## TP N°4 - Web API (programmation REST)

-partie 1 : l'api a été faites et testé 
-partie 3 : nous avons réussi a faire une partie des requètes en json cependant la serialisation se fait "à la main"
-partie 2 : les tokens ont été mis en place cependant suite a cette étape index.html ne charge plus correctement sur le navigateur (page blanche)
-partie 4 : faite pendant la partie 1

## TP 5 : Programmation côté client (requêtage asynchrone)

### 2.1. Single-Page Application
 **Déplier / replier le menu quand on clique sur son titre**
 `<div class="toggleTrigger" id="hamburger">`
 Dans app.js :

     $('.toggleTrigger').click(function(){ 
    	 $('.nav').slideToggle();
    	 $('#hamburger').toggleClass('trigger');
     });

 
**Mettre en place un mécanisme de routage qui affiche la vue correspondant au hash sélectionné**
Dans app.js : 

    window.addEventListener(
    	'hashchange',
    	() => { show(window.location.hash); 
    });

**Permettre l'édition d'éléments du texte affichés dans l'interface (pour la modification du nom de l'utilisateur**
Dans utils.js ( spécifique au changement de nom ) :

     changeNom.setAttribute('contentEditable','true');

On départ on avait décidé de mettre en place le changement grâce à une "class" qu'on mettait directement dans les balise qu'on voulait modifier, mais cela ne permettais pas d'envoyer la requête pour modifier son nom.

### 2.2 Templating
La fonction ( utils.js ) : 

    function completeTemplate(idScriptTemplate,idSection,data) {
    	 console.debug("Completing Template...");
    	 console.log(data);
    	 const template = document.getElementById(idScriptTemplate).innerHTML;
    	 var rendered = Mustache.render(template,data);
    	 document.getElementById(idSection).innerHTML = rendered;
    }

### 2.3. AJAX

Utilisation de fetch, les élements sont disponibles dans fetch.js.


## Tp7 
### Déploiement sur Tomcat
-   le temps de chargement de la page HTML initiale est de **250ms**
-   le temps d'affichage de l'app shell est de **255ms**
-   le temps d'affichage du chemin critique de rendu (CRP) est de **340ms**

### Déploiement sur NginX
-   le temps de chargement de *la page HTML initiale* est de **180ms**
-   le temps d'*affichage de l'app shell* est de **185ms** 
-   le temps d*'affichage du chemin critique de rendu (CRP)* est de **280ms**

### Améliorations
-   Temps de chargement de *la page HTML initiale* :  **28%**
-   Temps d'*affichage de l'app shell*  :  **28%**
-   Temps d*'affichage du chemin critique de rendu (CRP)*  : **18%**

### Calculs après chaque changement 
CDN déjà fait pour l'image du pied de page et les imports de librairies.

#### Changement 1 : "Defer" des scripts
-   le temps de chargement de *la page HTML initiale* est de **140**
-   le temps d'*affichage de l'app shell* est de **141** 
-   le temps d*'affichage du chemin critique de rendu (CRP)* est de **210**


### Améliorations changement 1
-   Temps de chargement de *la page HTML initiale* :  **22%** de plus
-   Temps d'*affichage de l'app shell*  :  **24%** de plus 
-   Temps d*'affichage du chemin critique de rendu (CRP)*  : **25%** de plus **39%** par rapport a l'original